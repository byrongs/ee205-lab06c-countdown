///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Byron Soriano <byrongs@hawaii.edu>
// @date   Feb 22 2022
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <time.h>
#include <stdbool.h>
#include <unistd.h>



int main(void) {
	struct tm value;

	value.tm_sec = 1;
	value.tm_min = 1;
	value.tm_hour = 1;
	value.tm_mday = 1;
	value.tm_mon = 1;
	value.tm_year = 122;
	value.tm_hour = 23;
	value.tm_wday = 1;
	value.tm_yday = 1;
	value.tm_isdst = 1;

   printf("Reference time: %s " , asctime(&value));
   

//counter loop for incrementing units of time
   while( value.tm_sec++ ) { 
	   if(value.tm_sec > 60 ) {
	       value.tm_sec = 0;
	       ++value.tm_min;
	       ++value.tm_sec;    
           }

	   if(value.tm_min > 60 ) {
	       value.tm_min = 0;
	       ++value.tm_hour;
	       ++value.tm_min;
	   }

	   if(value.tm_hour > 24 ) {
	       value.tm_hour = 0;
	       ++value.tm_yday;
	       ++value.tm_hour;
	   }

	   if(value.tm_yday > 365 ) {
	       value.tm_yday = 0;
	       ++value.tm_year;
	       ++value.tm_yday;
	   }


	   printf("Years : %d   Days : %d   Hours : %d   Minutes : %d   Seconds : %d\n",
	        value.tm_year,
		value.tm_yday,
		value.tm_hour,
		value.tm_min,
		value.tm_sec 
	   );

	   sleep(1);
           

   }
   return 0;

  
}


